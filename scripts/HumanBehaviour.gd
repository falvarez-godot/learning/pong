class_name HumanBehaviour
extends Node

var paddle: Paddle

func _init(paddle: Paddle):
	
	self.paddle = paddle

func _process(_delta: float)-> void:
	
	var velocity: Vector2 = Vector2.ZERO
	
	# @TODO buscar una forma más elegante de hacer esto
	if (paddle.id == 1):
		if (Input.is_action_pressed("p1_up")):
			velocity.y -= 1
		if (Input.is_action_pressed("p1_down")):
			velocity.y += 1
	elif (paddle.id == 2):
		if (Input.is_action_pressed("p2_up")): 
			velocity.y -= 1
		if (Input.is_action_pressed("p2_down")):
			velocity.y += 1
	else:
		return

	Messenger.PADDLE_MOVE.emit(paddle.id, velocity)
