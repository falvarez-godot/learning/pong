extends Node

# Lista de mensajes

signal BALL_POSITION(position: Vector2, delta)
signal PADDLE_BOUNCE(position: Vector2)
signal PLAYER_1_GOAL()
signal PLAYER_2_GOAL()
signal PADDLE_MOVE(id: int, velocity: Vector2)
signal WALL_BOUNCE()
