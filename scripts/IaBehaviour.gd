class_name IaBehaviour
extends Node

const AI_SPEED: int = 450
const POSITION_DELTA: int = 30

var paddle: Paddle

func _init(iaPaddle: Paddle):
	
	paddle = iaPaddle

func _ready()-> void:
	
	Messenger.connect("BALL_POSITION", followBall)
	
func followBall(ballPosition: Vector2, _delta: float)-> void:
	
	# @TODO Añadir niveles de dificultad para la IA, esto es, lo capaz o incapaz que es de seguir
	# la trayectoria de la pelota. Por ejemplo, saltándose pasos, equivocándose, o con menor 
	# velocidad de movimiento
	
	var velocity: Vector2 = Vector2.ZERO
	
	if abs(ballPosition.y - paddle.position.y) < POSITION_DELTA:
		return
	
	if ballPosition.y < paddle.position.y:
		velocity.y = -1
	if ballPosition.y > paddle.position.y:
		velocity.y = 1
		
	Messenger.PADDLE_MOVE.emit(paddle.id, velocity)
