class_name Ball extends CharacterBody2D

const CENTER = Vector2(640, 360)

var speed = 500

func _ready():
	
	Messenger.connect("PLAYER_1_GOAL", onPlayer1Goal)
	Messenger.connect("PLAYER_2_GOAL", onPlayer2Goal)
	
	reset(1 if (randi() % 2) else -1)

func _process(delta) -> void:
	
	Messenger.BALL_POSITION.emit(position, delta)

func _physics_process(delta) -> void:

	var collisionInfo = move_and_collide(velocity * delta)
	if (collisionInfo):
		var collider = collisionInfo.get_collider()
		if (collider.get_meta("type") == "paddle"):
			Messenger.PADDLE_BOUNCE.emit(position)
		elif (collider.get_meta("type") == "wall"):
			Messenger.WALL_BOUNCE.emit(position)
		velocity = velocity.bounce(collisionInfo.get_normal())

func onPlayer1Goal() -> void:
	reset(-1)
	
func onPlayer2Goal() -> void:
	reset(1)

func reset(velocityX: int) -> void:

	position = CENTER
	velocity.x = velocityX
	velocity.y = 1 if (randi() % 2) else -1
	velocity *= speed


