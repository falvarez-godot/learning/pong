class_name BallSoundManager extends Node

func _ready():
	
	$PaddleAudioStream.stream = preload("res://assets/sfx/paddle.ogg")
	$WallAudioStream.stream = preload("res://assets/sfx/wall.ogg")
	
	Messenger.connect("PADDLE_BOUNCE", onPaddleBounce)
	Messenger.connect("WALL_BOUNCE", onWallBounce)
	
func onPaddleBounce(position: Vector2):

	$PaddleAudioStream.position = position
	if $PaddleAudioStream.is_playing():
		$PaddleAudioStream.stop()
	$PaddleAudioStream.play()
	
func onWallBounce(position: Vector2):

	$WallAudioStream.position = position
	if $WallAudioStream.is_playing():
		$WallAudioStream.stop()
	$WallAudioStream.play()
