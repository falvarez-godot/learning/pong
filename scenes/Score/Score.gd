class_name Score extends StaticBody2D

var player1Score: int = 0
var player2Score: int = 0

func _ready():
	
	$GoalAudioStream.stream = preload("res://assets/sfx/goal.ogg")
	
	Messenger.connect("PLAYER_1_GOAL", onPlayer1Goal)
	Messenger.connect("PLAYER_2_GOAL", onPlayer2Goal)
	
	position = Vector2(640, 60)
	
	printScore($Player1Score, player1Score)
	printScore($Player2Score, player2Score)

func onPlayer1Goal() -> void:
	
	player1Score += 1
	printScore($Player1Score, player1Score)
	playGoalSound()
	
func onPlayer2Goal() -> void:
	
	player2Score += 1
	printScore($Player2Score, player2Score)
	playGoalSound()

func printScore(label: Label, score: int) -> void:
	label.text = str("%02d" % score)

func playGoalSound() -> void:

	if $GoalAudioStream.is_playing():
		$GoalAudioStream.stop()
	$GoalAudioStream.play()
