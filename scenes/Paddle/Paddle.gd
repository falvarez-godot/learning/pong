class_name Paddle
extends CharacterBody2D

const SPEED: int = 500

var id: int = 0

func _ready()-> void:

	Messenger.connect("PLAYER_1_GOAL", resetPosition)
	Messenger.connect("PLAYER_2_GOAL", resetPosition)
	Messenger.connect("PADDLE_MOVE", updateVelocity)

func _physics_process(delta: float)-> void:

	move_and_collide(velocity * delta)

func resetPosition()-> void:

	position.y = 360

func updateVelocity(paddleId: int, newVelocity: Vector2)-> void:
	
	if (paddleId == id):
		velocity = newVelocity * SPEED
