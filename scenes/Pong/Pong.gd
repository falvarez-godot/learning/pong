class_name Pong 
extends Node2D

const BallScene = preload("res://scenes/Ball/Ball.tscn")
const PaddleScene = preload("res://scenes/Paddle/Paddle.tscn")
const LeftPaddleGfx = preload("res://assets/gfx/paddle-blue.png")
const RightPaddleGfx = preload("res://assets/gfx/paddle-red.png")

func _ready() -> void:
	
	# @TODO esto habría que pedírselo al EntityManager
	
	var playerLeft = PaddleScene.instantiate()
	playerLeft.id = 1
	playerLeft.position = Vector2(100, 360)
	playerLeft.find_child("Sprite2D").texture = LeftPaddleGfx
	add_child(playerLeft)
	
	#var humanBehaviour = HumanBehaviour.new(playerLeft)
	#add_child(humanBehaviour)
	
	var iaBehaviour = IaBehaviour.new(playerLeft)
	add_child(iaBehaviour)
	
	var playerRight = PaddleScene.instantiate()
	playerRight.id = 2
	playerRight.position = Vector2(1180, 360)
	playerRight.find_child("Sprite2D").texture = RightPaddleGfx
	add_child(playerRight)
	
	#var humanBehaviour2 = HumanBehaviour.new(playerRight)
	#add_child(humanBehaviour2)
	
	var iaBehaviour2 = IaBehaviour.new(playerRight)
	add_child(iaBehaviour2)
	
	var ball = BallScene.instantiate()
	ball.position = Vector2(640, 360)
	add_child(ball)

func _on_left_goal_body_entered(_body) -> void:
	
	Messenger.PLAYER_2_GOAL.emit()

func _on_right_goal_body_entered(_body) -> void:
	
	Messenger.PLAYER_1_GOAL.emit()
