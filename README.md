# Compiler's Godot Pong

Versión simple de Pong para ir aprendiendo Godot 4

## Referencia

* https://www.youtube.com/watch?v=YCTPNRw1EXo
* https://github.com/indierama/PongTutorial/

## TO-DO

* Niveles de dificultad - IA del oponente
* Límite de puntuación - Final de partida
* Límite de tiempo - Final de partida
* Cambio de escenas - Menú de opciones:
	* Attract mode
	* 1/2 jugadores
* Splash screen
* Shader CRT
* Ventana / pantalla completa
* Gráficos personalizados
* Música de fondo
* Optimización del ejecutable generado
* Documento de explicación de la arquitectura

## Enlaces

Efectos de sonido descargados de [Pixabay](https://pixabay.com/)

## Recursos (volcar en página de recursos de Compiler Software)

* https://docs.godotengine.org/en/stable/tutorials/rendering/multiple_resolutions.html
* https://forum.godotengine.org/t/resize-viewport-on-change-display-orientation/1378
* https://godotshaders.com/
* https://www.gdquest.com/tutorial/godot/design-patterns/event-bus-singleton/
